package com.vendor.demo.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vendor.demo.model.Vendor;
import com.vendor.demo.repository.VendorRepository;
@Service
public class VendorService {
	@Autowired
	VendorRepository vendorRepository;

	public Vendor saveDetails(Vendor vendor) {
		
		
		return vendorRepository.save(vendor);
	}

}
