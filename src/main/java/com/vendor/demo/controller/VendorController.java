package com.vendor.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vendor.demo.model.Vendor;
import com.vendor.demo.service.VendorService;

@RestController
public class VendorController {
	@Autowired
	VendorService vendorService;
	
	
	@PostMapping("saveVendorDetails")
	public Vendor saveDetails(@RequestBody Vendor vendor) {
		
		return vendorService.saveDetails(vendor);
	}
	

}
