package com.vendor.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Vendor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long vendorId;
	private String fullVendorName;
	public long getVendorId() {
		return vendorId;
	}
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	public String getFullVendorName() {
		return fullVendorName;
	}
	public void setFullVendorName(String fullVendorName) {
		this.fullVendorName = fullVendorName;
	}
	

}
