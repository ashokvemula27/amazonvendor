package com.vendor.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vendor.demo.model.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {

}
